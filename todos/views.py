from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.shortcuts import redirect
from django.urls import reverse_lazy

from todos.models import TodoList, TodoItem


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemListView(ListView):
    model  = TodoItem
    templete_name = "todoitems/list.html"
