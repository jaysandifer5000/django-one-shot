from django.urls import path

from todos.views import (
    TodoListCreateView,
    TodoListDeleteView,
    TodoListUpdateView,
    TodoListDetailView,
    TodoListListView,  
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todos_delete"),
    path("new/", TodoListCreateView.as_view(), name="todos_new"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_edit"),
]